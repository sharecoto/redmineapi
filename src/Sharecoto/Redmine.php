<?php
/**
 * @author fujii@sharecoto.com
 */

namespace Sharecoto;
use \Zend\Http\Request;
use \Zend\Http\Client;

class Redmine
{
    /**
     * redmine baseUrl
     */
    private $url;

    /**
     * redmine api token
     */
    private $accessKey;

    /**
     * @param string|Zend\Uri\Http $url
     * @param string $token
     */
    public function __construct($url, $accessKey)
    {
        $this->setUrl($url)->setAccessKey($accessKey);
    }

    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    public function setAccessKey($accessKey)
    {
        $this->accessKey = $accessKey;
        return $this;
    }

    /**
     * チケットを登録する
     *
     * @param array $issue see. http://www.redmine.org/projects/redmine/wiki/Rest_Issues
     * @return \Zend\Http\Responce
     */
    public function createIssue(array $issue)
    {
        $result = $this->postToRedmine('issues.json', $issue);

        return $result;
    }

    /**
     * POSTする
     *
     * @see http://www.redmine.org/projects/redmine/wiki/Rest_Issues
     * @param string $path
     * @return Zend\Http\Responce
     */
    public function postToRedmine($path, array $data)
    {
        $url = $this->url .= $path;
        $request = $this->requestInstance($url, Request::METHOD_POST);
        $request->setContent(json_encode($data));

        $client = $this->clientInstance();
        $res = $client->dispatch($request);

        if (!$res->isSuccess()) {
            throw new RedmineException($res->toString(), $res->getStatusCode());
        }

        return $res;
    }

    /**
     * \Zend\Http\Requestのインスタンスを作る
     *
     * $param string $method
     */
    public function requestInstance($uri, $method = Request::METHOD_GET)
    {
        $request = new Request();
        $request->setMethod($method);
        $request->setUri($uri);
        $request->getHeaders()->addHeaders(array(
            'X-Redmine-API-Key' => $this->accessKey,
            'Content-Type'=> 'application/json'
        ));
        return $request;
    }

    /**
     * \Zend\Http\Clientのインスタンスを作る
     */
    public function clientInstance()
    {
        return new Client();
    }
}
