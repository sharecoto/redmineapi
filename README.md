# RedmineのREST Apiでいろいろするライブラリ

## 機能

* チケットを登録する（いまんとここれだけw）

## Requirements

* zendframework/zend-http 2.3.0以上
* PHP 5.3.23以上

## Composerでインストール

いまのところ、bitbucket上のリポジトリはプライベートなので、シェアコト関係者しかインストールできません。

当然packagist.orgにも登録してないので、以下のような感じで``composer.json``にリポジトリ情報を書く必要があります。

``composer.json``を編集したら、``$ composer.phar install``でOKです。

```
{
"repositories": [
    {
    "type": "vcs",
    "url": "git@bitbucket.org:sharecoto/redmineapi.git"
    }
  ],
  "require": {
    "sharecoto/redmineapi": "dev-master"
  }
}
```

## 使い方。

Redmineの設定で、「RESTによるWebサービスを有効にする」が有効になっているのが前提です。

### インスタンス

まずは、composerが生成してくれるautoloader.phpをrequireしておきます。通常は``vendor``ディレクトリの直下に配置されます。

APIアクセスキーは個人設定で取得できます。

```
<?php
require('path/to/vendor/autoloader.php');

$redmine = new \Sharecoto\Redmine(
    'http://redmine.sharecoto.net/', // redmineのベースURL
    'accessKey' // APIアクセスキー
);
```

### チケット(issue)を作成する

```
<?php
$issue = array(
    'issue' => array(
            'project_id' => $project_id, // integer
            'subject' => 'APIからチケット作成してみる',
            'description' => 'this is description',
        )
    );
$result = $redmine->createIssue($issue);

if ($result->isSuccess()) {
    echo 'Yeah!';
}
```

``createIssue()``が返す値はZend\Http\Responseクラスのインスタンスです。

なお、プロジェクトのIDを予め知っておく必要があるわけですが、手っ取り早く知るには、適当なプロジェクトの「概要」ページを開き、URLの末尾に``.xml``もしくは``.json``を追加します。

たとえば、SocialClipのプロジェクトでは以下のような出力になります。

http://redmine.sharecoto.net/projects/socialclip.xml

```
<project>
<id>9</id>
<name>社内 Social Clip</name>
<identifier>socialclip</identifier>
<description>Social Clip関連</description>
<homepage/>
<created_on>2014-05-07T09:11:47Z</created_on>
<updated_on>2014-05-22T01:50:15Z</updated_on>
</project>
```

### POSTでできることはたいていできる(はず)

postToRedmine()メソッドで、POSTでできることは、まあ、できると思います（未検証。コード見てね）。
redmine REST APIはjsonとxmlを許容しますが、いまのところ、このライブラリではjson限定です。
redmine APIのドキュメントは
http://www.redmine.org/projects/redmine/wiki/Rest_api
です。