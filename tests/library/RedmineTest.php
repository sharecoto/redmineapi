<?php




class RedmineTest extends PHPUnit_Framework_TestCase
{
    public function testInstance()
    {
        $config = require_once(__DIR__ . '/../redmine_config.php');

        $redmine = new \Sharecoto\Redmine(
            $config['url'],
            $config['accessKey']
        );

        $issue = array(
            'issue' => array(
                    'project_id' => $config['project_id'],
                    'subject' => 'APIからチケット作成してみる',
                    'description' => 'this is description',
                )
            );
        $result = $redmine->createIssue($issue);
    }
}
