<?php
/**
 * テストスクリプトで実際にRedmineを操作してしまう都合上、
 * 荒らしてもいいRedmineで試してください。
 * 各パラメータを適切に書き換えて、
 * redmine_config.phpにリネームして使ってください。
 * なお、redmine_config.phpはgitの管理外となります。
 *
 * accessKeyについては、redmineの「管理」>「設定」から「認証」
 * で「RESTによるWebサービスを有効にする」にしたあと、
 * 「個人設定」で取得できます。
 */

return array (
    'url' => 'http://localhost:3000/', //荒らしてもいい実験用のredmineのURL
    'accessKey' => '0c59726256f8e0c08d95012973da768cadbe742c',
    'project_id' => 1
);
